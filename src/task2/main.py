from src.task2.lib.MonteCarloPI import MonteCarloPI


def main():
    amount_input = int(input("Enter the number of points to calculate the number of PI >>> "))
    print(f"Pi is {MonteCarloPI(amount_input).simulate()}")


if __name__ == "__main__":
    main()
