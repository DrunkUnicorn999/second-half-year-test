from numpy import median


class Filter(object):
    def __init__(self, data=None, window_size=None):
        self.data = data
        self.window_size = window_size
        pass

    def filter(self):
        raise NotImplementedError


class SMAFilter(Filter):
    def filter(self):
        result = []
        for i in range(len(self.data)):
            if i < self.window_size-1:
                result.append(None)
            else:
                result.append(1 / self.window_size * sum(self.data[i + 1 - self.window_size:i + 1]))
        return result


class EMAFilter(Filter):
    def filter(self):
        result = []
        alpha = 2 / (self.window_size + 1)
        for i in range(len(self.data)):
            if i < self.window_size-1:
                result.append(None)
            elif len(result) == self.window_size-1:
                result.append(SMAFilter(self.data[:self.window_size], self.window_size).filter()[-1])
            else:
                result.append(alpha*self.data[i] + (1-alpha) * result[-1])
        return result


class MedianFilter(Filter):
    def filter(self):
        result = []
        for i in range(len(self.data)):
            if i < self.window_size-1:
                result.append(None)
            else:
                result.append(median(self.data[i + 1 - self.window_size:i + 1]))
        return result
